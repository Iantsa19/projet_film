package film.Controller;

import DAO.HibernateDAO;
import film.Model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class DetailSceneController {

    @Autowired
    HibernateDAO dao;

    public HibernateDAO getDao() {
        return dao;
    }

    public void setDao(HibernateDAO dao) {
        this.dao = dao;
    }

    @PostMapping("/DetailScene")
    public String ListeDetailScene(@ModelAttribute V_detail detail, Model model){
        try {
            Object[] element = dao.where(detail);
            V_detail[] listeDetail = new V_detail[element.length];
            for (int i = 0; i < element.length; i++) {
                V_detail d = (V_detail) element[i];
                listeDetail[i] = d;
            }
            model.addAttribute("listeDetail", listeDetail);
            Object[] element_type = dao.findAll(new Type_action());
            Type_action[] listeType = new Type_action[element_type.length];
            for (int i = 0; i < element_type.length; i++) {
                Type_action t = (Type_action) element_type[i];
                listeType[i] = t;
            }
            model.addAttribute("listeType", listeType);
            Object[] element_personnage = dao.findAll(new Personnage());
            Personnage[] listePersonnage = new Personnage[element_personnage.length];
            for (int i = 0; i < element_personnage.length; i++) {
                Personnage p = (Personnage) element_personnage[i];
                listePersonnage[i] = p;
            }
            model.addAttribute("listePersonnage", listePersonnage);
            Object[] element_emotion = dao.findAll(new Emotion());
            Emotion[] listeEmotion = new Emotion[element_emotion.length];
            for (int i = 0; i < element_emotion.length; i++) {
                Emotion p = (Emotion) element_emotion[i];
                listeEmotion[i] = p;
            }
            Scene sn = new Scene();
            sn.setId(detail.getId_scene());
            Scene scene = (Scene) dao.getById(sn);
            model.addAttribute("listeEmotion", listeEmotion);
            model.addAttribute("id_scene", detail.getId_scene());
            model.addAttribute("scene", scene);
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return "listeDetail";
    }

    @PostMapping("/DetailScenes")
    public String createDetailScene(@ModelAttribute V_detail detail, Model model){
        try {
            DetailScene detailScene = new DetailScene();
            detailScene.setId_scene(detail.getId_scene());
            detailScene.setId_type_action(detail.getId_type_action());
            detailScene.setId_personnage(detail.getId_personnage());
            detailScene.setIntitule(detail.getIntitule());
            detailScene.setId_emotion(detail.getId_emotion());
            detailScene.setDuree(detail.getDuree());
            dao.save(detailScene);

            SceneModel search = new SceneModel();
            search.setId(detail.getId_scene());
            SceneModel scene = (SceneModel) dao.getById(search);
            if(scene.getId_statu()==1){
                scene.setId_statu(2);
                dao.update(scene);
            }

            V_detail dt = new V_detail();
            dt.setId_scene(detail.getId_scene());

            Object[] element = dao.where(dt);
            V_detail[] listeDetail = new V_detail[element.length];
            for (int i = 0; i < element.length; i++) {
                V_detail d = (V_detail) element[i];
                listeDetail[i] = d;
            }
            model.addAttribute("listeDetail", listeDetail);
            Object[] element_type = dao.findAll(new Type_action());
            Type_action[] listeType = new Type_action[element_type.length];
            for (int i = 0; i < element_type.length; i++) {
                Type_action t = (Type_action) element_type[i];
                listeType[i] = t;
            }
            model.addAttribute("listeType", listeType);
            Object[] element_personnage = dao.findAll(new Personnage());
            Personnage[] listePersonnage = new Personnage[element_personnage.length];
            for (int i = 0; i < element_personnage.length; i++) {
                Personnage p = (Personnage) element_personnage[i];
                listePersonnage[i] = p;
            }
            model.addAttribute("listePersonnage", listePersonnage);
            Object[] element_emotion = dao.findAll(new Emotion());
            Emotion[] listeEmotion = new Emotion[element_emotion.length];
            for (int i = 0; i < element_emotion.length; i++) {
                Emotion p = (Emotion) element_emotion[i];
                listeEmotion[i] = p;
            }
            Scene sn = new Scene();
            sn.setId(detail.getId_scene());
            Scene scenes = (Scene) dao.getById(sn);
            model.addAttribute("listeEmotion", listeEmotion);
            model.addAttribute("id_scene", detail.getId_scene());
            model.addAttribute("scene", scenes);
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return "listeDetail";
    }

    @PostMapping("/deleteDetail")
    public String deleteDetailScene(@ModelAttribute V_detail detail, Model model){
        try {
            DetailScene de = new DetailScene();
            de.setId(detail.getId());
            dao.delete(de);

            V_detail vd = new V_detail();
            vd.setId_scene(detail.getId_scene());

            Object[] element = dao.where(vd);
            V_detail[] listeDetail = new V_detail[element.length];
            for (int i = 0; i < element.length; i++) {
                V_detail d = (V_detail) element[i];
                listeDetail[i] = d;
            }
            model.addAttribute("listeDetail", listeDetail);
            Object[] element_type = dao.findAll(new Type_action());
            Type_action[] listeType = new Type_action[element_type.length];
            for (int i = 0; i < element_type.length; i++) {
                Type_action t = (Type_action) element_type[i];
                listeType[i] = t;
            }
            model.addAttribute("listeType", listeType);
            Object[] element_personnage = dao.findAll(new Personnage());
            Personnage[] listePersonnage = new Personnage[element_personnage.length];
            for (int i = 0; i < element_personnage.length; i++) {
                Personnage p = (Personnage) element_personnage[i];
                listePersonnage[i] = p;
            }
            model.addAttribute("listePersonnage", listePersonnage);
            Object[] element_emotion = dao.findAll(new Emotion());
            Emotion[] listeEmotion = new Emotion[element_emotion.length];
            for (int i = 0; i < element_emotion.length; i++) {
                Emotion p = (Emotion) element_emotion[i];
                listeEmotion[i] = p;
            }
            Scene sn = new Scene();
            sn.setId(detail.getId_scene());
            Scene scene = (Scene) dao.getById(sn);
            model.addAttribute("listeEmotion", listeEmotion);
            model.addAttribute("id_scene", detail.getId_scene());
            model.addAttribute("scene", scene);
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return "listeDetail";
    }

}
