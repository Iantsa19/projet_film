package film.Controller;

import DAO.HibernateDAO;
import film.Model.Emotion;
import film.Model.Personnage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class EmotionController {

    @Autowired
    HibernateDAO dao;

    public HibernateDAO getDao() {
        return dao;
    }

    public void setDao(HibernateDAO dao) {
        this.dao = dao;
    }

    @GetMapping("/Emotions")
    public String listeEmotions(Model model){
        Object[] element = dao.findAll(new Emotion());
        Emotion[] listeEmotion = new Emotion[element.length];
        for (int i = 0; i < element.length; i++) {
            Emotion emotion = (Emotion) element[i];
            listeEmotion[i] = emotion;
        }
        model.addAttribute("listeEmotion", listeEmotion);
        return "listeEmotion";
    }

    @PostMapping("/Emotions")
    public String insererPersonnage(@ModelAttribute Emotion emotion, Model model){
        dao.save(emotion);
        Object[] element = dao.findAll(new Emotion());
        Emotion[] listeEmotion = new Emotion[element.length];
        for (int i = 0; i < element.length; i++) {
            Emotion e = (Emotion) element[i];
            listeEmotion[i] = e;
        }
        model.addAttribute("listeEmotion", listeEmotion);
        return "listeEmotion";
    }

    @PostMapping("/deleteEmotion")
    public String deleteEmotion(@ModelAttribute Emotion emotion, Model model){
        dao.delete(emotion);
        Object[] element = dao.findAll(new Emotion());
        Emotion[] listeEmotion = new Emotion[element.length];
        for (int i = 0; i < element.length; i++) {
            Emotion e = (Emotion) element[i];
            listeEmotion[i] = e;
        }
        model.addAttribute("listeEmotion", listeEmotion);
        return "listeEmotion";
    }

}
