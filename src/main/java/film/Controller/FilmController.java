package film.Controller;

import DAO.HibernateDAO;
import film.Model.Film;
import film.Model.ToPdf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class FilmController {

    @Autowired
    HibernateDAO dao;

    public HibernateDAO getDao() {
        return dao;
    }

    public void setDao(HibernateDAO dao) {
        this.dao = dao;
    }

    @GetMapping("/Films")
    public String listeFilm(Model model){
        Object[] element = dao.findAll(new Film());
        Film[] listeFilm = new Film[element.length];
        for (int i = 0; i < element.length; i++) {
            Film film = (Film) element[i];
            listeFilm[i] = film;
        }
        model.addAttribute("listeFilm", listeFilm);
        return "listeFilm";
    }


    @PostMapping("/Films")
    public String insererFilm(@ModelAttribute Film film, Model model){
        dao.save(film);
        Object[] element = dao.findAll(new Film());
        Film[] listeFilm = new Film[element.length];
        for (int i = 0; i < element.length; i++) {
            Film f = (Film) element[i];
            listeFilm[i] = f;
        }
        model.addAttribute("listeFilm", listeFilm);
        return "listeFilm";
    }

    @PostMapping("/deleteFilm")
    public String deleteFilm(@ModelAttribute Film film, Model model){
        dao.delete(film);
        Object[] element = dao.findAll(new Film());
        Film[] listeFilm = new Film[element.length];
        for (int i = 0; i < element.length; i++) {
            Film f = (Film) element[i];
            listeFilm[i] = f;
        }
        model.addAttribute("listeFilm", listeFilm);
        return "listeFilm";
    }
}
