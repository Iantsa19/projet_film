package film.Controller;

import DAO.HibernateDAO;
import film.Model.Emotion;
import film.Model.Personnage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class PersonnageController {

    @Autowired
    HibernateDAO dao;

    public HibernateDAO getDao() {
        return dao;
    }

    public void setDao(HibernateDAO dao) {
        this.dao = dao;
    }

    @GetMapping("/Personnages")
    public String listePersonnages(Model model){
        Object[] element = dao.findAll(new Personnage());
        Personnage[] listePersonnage = new Personnage[element.length];
        for (int i = 0; i < element.length; i++) {
            Personnage personnage = (Personnage) element[i];
            listePersonnage[i] = personnage;
        }
        model.addAttribute("listePersonnage", listePersonnage);
        return "listePersonnage";
    }

    @PostMapping("/Personnages")
    public String insererPersonnage(@ModelAttribute Personnage personnage, Model model){
        dao.save(personnage);
        Object[] element = dao.findAll(new Personnage());
        Personnage[] listePersonnage = new Personnage[element.length];
        for (int i = 0; i < element.length; i++) {
            Personnage p = (Personnage) element[i];
            listePersonnage[i] = p;
        }
        model.addAttribute("listePersonnage", listePersonnage);
        return "listePersonnage";
    }

    @PostMapping("/deletePersonnage")
    public String deletePersonnage(@ModelAttribute Personnage personnage, Model model){
        dao.delete(personnage);
        Object[] element = dao.findAll(new Personnage());
        Personnage[] listePersonnage = new Personnage[element.length];
        for (int i = 0; i < element.length; i++) {
            Personnage p = (Personnage) element[i];
            listePersonnage[i] = p;
        }
        model.addAttribute("listePersonnage", listePersonnage);
        return "listePersonnage";
    }

}
