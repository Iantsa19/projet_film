package film.Controller;

import DAO.HibernateDAO;
import film.Model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Controller
public class PlanningController {

    @Autowired
    HibernateDAO dao;
    JdbcTemplate template;
    JourPlanning[] jourPlannings;
    List<V_scene> scenesAplanifer;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        template = new JdbcTemplate(dataSource);
    }

    public HibernateDAO getDao() {
        return dao;
    }

    public void setDao(HibernateDAO dao) {
        this.dao = dao;
    }


    @GetMapping("/choix-film-general")
    public String chooseMovieGen(Model model){
        Object[] element = dao.findAll(new Film());
        Film[] listefilm = new Film[element.length];
        for (int i = 0; i < element.length; i++) {
            Film film = (Film) element[i];
            listefilm[i] = film;
        }
        model.addAttribute("listefilm", listefilm);
        return "choixFilmGeneral";
    }
    @GetMapping("/choix-film")
    public String chooseMovie(Model model){
        Object[] element = dao.findAll(new Film());
        Film[] listefilm = new Film[element.length];
        for (int i = 0; i < element.length; i++) {
            Film film = (Film) element[i];
            listefilm[i] = film;
        }
        model.addAttribute("listefilm", listefilm);
        return "choixFilm";
    }
    @PostMapping("/choix-film")
    public String choosedMovie(@ModelAttribute V_scene scene, Model model){
//        Object[] element;
//        scene.setId_statu(3);   // statut terminé = 3
        try {
//            element = dao.where(scene);
            V_scene[] listeScene = findByStatuAndFilm(3, scene.getId_film());
//            for (int i = 0; i < element.length; i++) {
//                V_scene v_scene = (V_scene) element[i];
//                listeScene[i] = v_scene;
//            }
            model.addAttribute("listeScene", listeScene);
            return "listeSceneTermine";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    @PostMapping("/choix-film-general")
    public String choosedMovieGeneral( Model model){
        try {

            return "choixDate";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    @PostMapping("/valider-planning")
    public String valider(Model model, @RequestParam("i") int indexOfDate, @RequestParam("j[]") int[] j, @RequestParam("id_plateau[]") String[] id_plateau) {
        try {
            PlateauIndisponible indisponible = new PlateauIndisponible();
            PersonnageIndisponible perso_indisponible = new PersonnageIndisponible();
            SceneModel scene = new SceneModel();
            Date date = jourPlannings[indexOfDate].getDate();
            for (int i = 0; i < j.length; i++) {
                indisponible.setId_plateau(Integer.parseInt(id_plateau[i]));
                indisponible.setObservation("Planifie pour le " + date.toString().substring(0, 10));
                indisponible.setDate(date);
                System.out.println(date);
                dao.save(indisponible);
                for (int k = 0; k < jourPlannings[indexOfDate].getScenes().get(j[i]).getPersonnages().length; k++) {
                    perso_indisponible.setId_personnage(jourPlannings[indexOfDate].getScenes().get(j[i]).getPersonnages()[k].getId());
                    perso_indisponible.setObservation("Tournage pour le " + date.toString().substring(0, 10));
                    perso_indisponible.setDate(date);
                    dao.save(perso_indisponible);
                }
                jourPlannings[indexOfDate].getScenes().get(j[i]).getStatu().setId(4); //statu planifié
                jourPlannings[indexOfDate].getScenes().get(j[i]).setId_statu(4); //statu planifié
                jourPlannings[indexOfDate].getScenes().get(j[i]).setDate_tournage(date);
                scene.setId(jourPlannings[indexOfDate].getScenes().get(j[i]).getId());
                scene = (SceneModel) dao.getById(scene);
                scene.setId_statu(4);
                scene.setDate_tournage(date);
                dao.update(scene);
            }
            model.addAttribute("dates", this.jourPlannings);
            model.addAttribute("scenesAplanifier", this.scenesAplanifer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "propositionPlanning";
    }

    @PostMapping("/add-scene-to-date")
    public String addScene(Model model, @RequestParam("indexDate") int i, @RequestParam("j[]") int[] j) {
        try {
            if (!scenesAplanifer.isEmpty()) {
                boolean marina[] = new boolean[j.length];
                for (int k = 0; k < j.length; k++) {
                    marina[k] = false;
                    if (jourPlannings[i].getResteDuree() >= scenesAplanifer.get(j[k]).getDuree() && isPlateauDispo(jourPlannings[i], scenesAplanifer.get(j[k]).getPlateau().getId()) && isActeurDispo(jourPlannings[i], scenesAplanifer.get(j[k]))) {
                        this.jourPlannings[i].getScenes().add(scenesAplanifer.get(j[k]));
                        this.jourPlannings[i].setResteDuree(jourPlannings[i].getResteDuree() - scenesAplanifer.get(j[k]).getDuree());
                        marina[k] = true;
                    }
                }
                System.out.println("i: " + i);
                for (int k = 0; k < j.length; k++) {
                    if (marina[k]) {
                        scenesAplanifer.remove(j[k]);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("dates", this.jourPlannings);
        model.addAttribute("scenesAplanifier", this.scenesAplanifer);
        return "propositionPlanning";
    }

    @GetMapping("/remove-scene-to-date/{indexOfDate}/{indexOfScene}")
    public String removeScene(Model model, @PathVariable("indexOfDate") int indexOfDate, @PathVariable("indexOfScene") int indexOfScene) {
        try {
//            jourPlannings[indexOfDate].getScenes().get(indexOfScene).getStatu().setId(3);
//            jourPlannings[indexOfDate].getScenes().get(indexOfScene).setId_statu(3);
            this.scenesAplanifer.add(jourPlannings[indexOfDate].getScenes().get(indexOfScene));
//            SceneModel scene = new SceneModel();
//            scene.setId(jourPlannings[indexOfDate].getScenes().get(indexOfScene).getId());
//            scene = (SceneModel) dao.getById(scene);
//            scene.setId_statu(3); //statu terminé
//            scene.setDate_tournage(null);
//            dao.update(scene);
            this.jourPlannings[indexOfDate].setResteDuree(jourPlannings[indexOfDate].getResteDuree() + jourPlannings[indexOfDate].getScenes().get(indexOfScene).getDuree());
            this.jourPlannings[indexOfDate].getScenes().remove(indexOfScene);
            model.addAttribute("dates", this.jourPlannings);
            model.addAttribute("scenesAplanifier", this.scenesAplanifer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "propositionPlanning";
    }

    @PostMapping("/add-planning")
    public String add(Model model, @RequestParam("d1") String d1, @RequestParam("d2") String d2, @RequestParam("id_scene[]") String[] id_scene) {
        V_scene vs = new V_scene();
        JourPlanning[] dates = null;
        List<V_scene> scenes = new ArrayList<V_scene>();
        try {
            for (int i = 0; i < id_scene.length; i++) {
                vs.setId(Integer.parseInt(id_scene[i]));
                scenes.add((V_scene) dao.getById(vs));
            }
            scenes = prioriteScene(scenes);
            dates = getDateBetween(d1, d2);
            for (int i = 0; i < dates.length; i++) {
                int total_Duree = 0;
                V_scene v_old = new V_scene();
                v_old.setDate_tournage(dates[i].getDate());
                Object[] olds = dao.where(v_old);
                if (olds != null) {
                    V_scene[] v_olds = new V_scene[olds.length];
                    for (int j = 0; j < v_olds.length; j++) {
                        v_olds[j] = (V_scene) olds[j];
                    }
                    dates[i].setOldScenes(v_olds);
                }
                int j = 0;
                while (total_Duree <= dates[i].getMAX_DUREE()) {
                    if (scenes.size() == j || scenes.isEmpty() || scenes.get(j).getDuree() >= dates[i].getResteDuree()) {
                        break;
                    }
                    if (isPlateauDispo(dates[i], scenes.get(j).getPlateau().getId()) && isActeurDispo(dates[i], scenes.get(j))) {
                        dates[i].getScenes().add(scenes.get(j));
                        total_Duree += scenes.get(j).getDuree();
                        dates[i].setResteDuree(dates[i].getResteDuree() - scenes.get(j).getDuree());
                        scenes.remove(j);
                    } else {
                        j++;
                    }
                }
            }
            this.jourPlannings = dates;
            this.scenesAplanifer = scenes;
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("dates", dates);
        model.addAttribute("scenesAplanifier", scenes);
        return "propositionPlanning";
    }
    @PostMapping("/Planning")
    public String add(Model model, @RequestParam("d1") String d1, @RequestParam("d2") String d2, HttpServletRequest request) {
        V_scene vs = new V_scene();
        JourPlanning[] dates = null;
        List<V_scene> scenes = new ArrayList<V_scene>();
        HttpSession session= request.getSession();
        try {

            dates = getDateBetween(d1, d2);
            for (int i = 0; i < dates.length; i++) {
                int total_Duree = 0;
                V_scene v_old = new V_scene();
                v_old.setDate_tournage(dates[i].getDate());
                Object[] olds = dao.where(v_old);
                if (olds != null) {
                    V_scene[] v_olds = new V_scene[olds.length];
                    for (int j = 0; j < v_olds.length; j++) {
                        v_olds[j] = (V_scene) olds[j];
                    }
                    dates[i].setOldScenes(v_olds);
                }
                for(V_scene s:dates[i].getScenes()){
                    s.setDetails(getDetails(s.getId()));
                }
                int j = 0;

            }
            session.setAttribute("contenu",dates);

        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("dates", dates);
        model.addAttribute("scenesPlanifiees", scenes);
        return "PlanningGeneral";
    }

    Date stringToDateSql(String dateString) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date utilDate = dateFormat.parse(dateString);
        return new Date(utilDate.getTime());
    }

    JourPlanning[] getDateBetween(String d1, String d2) throws Exception {
        LocalDate date = LocalDate.parse(d1);
        LocalDate endDate = LocalDate.parse(d2);
        List<LocalDate> feries = getFeries(date.getYear(), endDate.getYear());
        List<JourPlanning> jp = new ArrayList<JourPlanning>();
        while (date.isBefore(endDate) || date.isEqual(endDate)) {
            if (date.getDayOfWeek() != DayOfWeek.SATURDAY && date.getDayOfWeek() != DayOfWeek.SUNDAY && !feries.contains(date)) {
                JourPlanning jour = new JourPlanning();
                jour.setDate(Date.valueOf(date));
                jp.add(jour);
            }
            date = date.plus(1, ChronoUnit.DAYS);
        }

        JourPlanning[] val = new JourPlanning[jp.size()];
        for (int i = 0; i < val.length; i++) {
            val[i] = new JourPlanning();
            val[i] = jp.get(i);
        }
        return val;
    }

    private List<LocalDate> getFeries(int a1, int a2) {
        List<LocalDate> rep = new ArrayList<LocalDate>();
        // to update
        rep.add(LocalDate.of(2023, 3, 8)); // Journée internationale des droits des femmes
        rep.add(LocalDate.of(2023, 3, 29));
        rep.add(LocalDate.of(2023, 5, 18));
        rep.add(LocalDate.of(2023, 5, 28));
        rep.add(LocalDate.of(2023, 4, 1));
        rep.add(LocalDate.of(2023, 4, 9));
        rep.add(LocalDate.of(2023, 6, 26));
        return rep;
    }

    public boolean isPlateauDispo(JourPlanning jp, int id_plateau) throws Exception {
        String sql = "SELECT count(id) FROM plateau_indisponible WHERE id_plateau = " + id_plateau + " AND date='" + jp.getDate().toString() + "'";
        System.out.println(sql);
        Integer count = template.query(sql, new ResultSetExtractor<Integer>() {
            @Override
            public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
                int rep = 0;
                if (rs.next()) {
                    rep = rs.getInt("count");
                }
                return rep;
            }
        });

        if (count == 0 || count == null) return true;
        return false;
    }

    public Personnage[] getPersonnagesScene(V_scene scene) throws Exception {
        String sql = "SELECT id_personnage FROM detailscene WHERE id_scene = " + scene.getId() +" GROUP BY id_personnage";
        System.out.println(sql);
        List<Personnage> acteurs = template.query(sql, new ResultSetExtractor<List<Personnage>>() {
            @Override
            public List<Personnage> extractData(ResultSet rs) throws SQLException, DataAccessException {
                List<Personnage> rep = new ArrayList<Personnage>();
                while (rs.next()) {
                    Personnage vss = new Personnage();
                    vss.setId(rs.getInt("id_personnage"));
                    rep.add(vss);
                }
                return rep;
            }
        });

        Personnage[] val = new Personnage[acteurs.size()];
        for (int i = 0; i < val.length; i++) {
            val[i] = new Personnage();
            val[i] = acteurs.get(i);
        }
        scene.setPersonnages(val);
        return val;
    }
    

    public boolean isActeurDispo(JourPlanning jp, V_scene scene) throws Exception {
        Personnage[] acteurs = getPersonnagesScene(scene);
        ResultSetExtractor<Integer> result = new ResultSetExtractor<Integer>() {
            @Override
            public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
                int rep = 0;
                if (rs.next()) {
                    rep = rs.getInt("count");
                }
                return rep;
            }
        };
        for (int i = 0; i < acteurs.length; i++) {
            String sql = "SELECT count(id) FROM personnage_indisponible WHERE id_personnage = " + acteurs[i].getId() + " AND date='" + jp.getDate().toString() + "'";
            System.out.println(sql);
            Integer count = template.query(sql, result);
            if (count != 0) {
                return false;
            }
        }

        return true;
    }

    
    

    private List<V_scene> prioriteScene(List<V_scene> l) {
        //

        return l;
    }
        


    @GetMapping("/pdf")
    public String pdf(Model model,HttpServletRequest request){
        HttpSession session= request.getSession();
        JourPlanning[] val=(JourPlanning[] )session.getAttribute("contenu");
        ToPdf pd=new ToPdf(val);
        return choosedMovieGeneral(model);
    }

    public List<V_detail> getDetails(int id_scene) throws Exception {
        List<V_detail> val=new ArrayList<V_detail>();
        String sql = "SELECT *  FROM V_detail WHERE id_scene = " + id_scene;
        System.out.println(sql);
        List<V_detail> count = template.query(sql, new ResultSetExtractor<List<V_detail>>() {
            @Override
            public List<V_detail> extractData(ResultSet rs) throws SQLException, DataAccessException {

                List<V_detail> rep=new ArrayList<V_detail>();

                while (rs.next()) {
                    V_detail d=new V_detail();
                    // id | id_scene | id_type_action | id_personnage | intitule | id_emotion | duree | nom_type_action |  nom_acteur  | nom_emotion
                    d.setId(rs.getInt("id"));
                    d.setId_scene(rs.getInt("id_scene"));
                    d.setId_type_action(rs.getInt("id_type_action"));
                    d.setId_personnage(rs.getInt("id_personnage"));
                    d.setIntitule(rs.getString("intitule"));
                    d.setId_emotion(rs.getInt("id_emotion"));
                    d.setDuree(rs.getInt("duree"));
                    d.setNom_type_action(rs.getString("nom_type_action"));
                    d.setNom_acteur(rs.getString("nom_acteur"));
                    d.setNom_emotion(rs.getString("nom_emotion"));
                    rep.add(d);
                }
                return rep;
            }
        });

        return count;
    }

    public V_scene[] findByStatuAndFilm(int id_statu, int id_film) throws Exception {
        String sql = "SELECT * FROM v_scene WHERE id_statu = " + id_statu + " AND id_film=" + id_film + " ORDER BY id_plateau,id";
        System.out.println(sql);
        List<V_scene> rep = template.query(sql, new ResultSetExtractor<List<V_scene>>() {
            @Override
            public List<V_scene> extractData(ResultSet rs) throws SQLException, DataAccessException {
                List<V_scene> rep = new ArrayList<V_scene>();
                while (rs.next()) {
                    V_scene vss = new V_scene();
                    vss.setId(rs.getInt("id"));
                    vss.setDate_tournage(rs.getDate("date_tournage"));
                    vss.setNom_scene(rs.getString("nom_scene"));
                    vss.setDuree(rs.getInt("duree"));
                    //
                    rep.add(vss);
                }
                return rep;
            }
        });
        V_scene[] val = new V_scene[rep.size()];
        for (int i = 0; i < val.length; i++) {
            val[i] = new V_scene();
            val[i] = rep.get(i);
        }
        return val;
    }


    public List<DetailScene> getDetailscene(int id_scene) throws Exception {
        List<DetailScene> val=new ArrayList<DetailScene>();
        String sql = "SELECT *  FROM detailscene WHERE id_scene = " + id_scene;
        System.out.println(sql);
        List<DetailScene> count = template.query(sql, new ResultSetExtractor<List<DetailScene>>() {
            @Override
            public List<DetailScene> extractData(ResultSet rs) throws SQLException, DataAccessException {

                List<DetailScene> rep=new ArrayList<DetailScene>();

                while (rs.next()) {
                    DetailScene d=new DetailScene();
                    //id | id_scene | id_type_action | id_personnage | intitule | id_emotion | duree
                    d.setId(rs.getInt("id"));
                    d.setId_scene(rs.getInt("id_scene"));
                    d.setId_type_action(rs.getInt("id_type_action"));
                    d.setId_personnage(rs.getInt("id_personnage"));
                    d.setIntitule(rs.getString("intitule"));
                    d.setId_emotion(rs.getInt("id_emotion"));
                    d.setDuree(rs.getInt("duree"));

                    rep.add(d);
                }
                return rep;
            }
        });

        return count;
    }
}
