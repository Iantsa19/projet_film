package film.Controller;

import DAO.HibernateDAO;
import film.Model.Plateau;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class PlateauController {

    @Autowired
    HibernateDAO dao;

    public HibernateDAO getDao() {
        return dao;
    }

    public void setDao(HibernateDAO dao) {
        this.dao = dao;
    }

    @GetMapping("/Plateaux")
    public String listePlateau(Model model){
        Object[] element = dao.findAll(new Plateau());
        Plateau[] listeplateau = new Plateau[element.length];
        for (int i = 0; i < element.length; i++) {
            Plateau ecole = (Plateau) element[i];
            listeplateau[i] = ecole;
        }
        model.addAttribute("listeplateau", listeplateau);
        return "listePlateau";
    }

    @PostMapping("/Plateaux")
    public String insererPlateau(@ModelAttribute Plateau plateau,Model model){
        dao.save(plateau);
        Object[] element = dao.findAll(new Plateau());
        Plateau[] listeplateau = new Plateau[element.length];
        for (int i = 0; i < element.length; i++) {
            Plateau ecole = (Plateau) element[i];
            listeplateau[i] = ecole;
        }
        model.addAttribute("listeplateau", listeplateau);
        return "listePlateau";
    }

    @PostMapping("/deletePlateau")
    public String deletePlateau(@ModelAttribute Plateau plateau,Model model){
        dao.delete(plateau);
        Object[] element = dao.findAll(new Plateau());
        Plateau[] listeplateau = new Plateau[element.length];
        for (int i = 0; i < element.length; i++) {
            Plateau ecole = (Plateau) element[i];
            listeplateau[i] = ecole;
        }
        model.addAttribute("listeplateau", listeplateau);
        return "listePlateau";
    }

}
