package film.Controller;

import DAO.HibernateDAO;
import film.Model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class SceneController {

    @Autowired
    HibernateDAO dao;

    public HibernateDAO getDao() {
        return dao;
    }

    public void setDao(HibernateDAO dao) {
        this.dao = dao;
    }

    @PostMapping("/Scenes")
    public String ListeScene(@ModelAttribute V_scene scene,Model model){
        try {
            V_scene ls = new V_scene();
            ls.setId_film(scene.getId_film());
            System.out.println("iddddd: "+ls.getId_film());
            Object[] element = dao.where(ls);
            V_scene[] listeScene = new V_scene[element.length];
            for (int i = 0; i < element.length; i++) {
                V_scene ecole = (V_scene) element[i];
                listeScene[i] = ecole;
            }
            Object[] plateau_element = dao.findAll(new Plateau());
            Plateau[] listePlateau = new Plateau[plateau_element.length];
            for (int i = 0; i < plateau_element.length; i++) {
                Plateau plateau = (Plateau) plateau_element[i];
                listePlateau[i] = plateau;
            }
            Object[] auteur_element = dao.findAll(new Auteur());
            Auteur[] listeAuteur = new Auteur[auteur_element.length];
            for (int i = 0; i < auteur_element.length; i++) {
                Auteur auteur = (Auteur) auteur_element[i];
                listeAuteur[i] = auteur;
            }
            model.addAttribute("listeAuteur", listeAuteur);
            model.addAttribute("listePlateau", listePlateau);
            model.addAttribute("listeScene", listeScene);
            model.addAttribute("id_film",scene.getId_film());
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return "listeScene";
    }

    @PostMapping("/Scene")
    public String createScene(@ModelAttribute V_scene scene,Model model){
        try {
            Scene sc = new Scene();
            sc.setNom_scene(scene.getNom_scene());

            Film film = new Film();
            film.setId(scene.getId_film());
            sc.setFilm(film);

            Plateau plateau = new Plateau();
            plateau.setId(scene.getId_plateau());
            sc.setPlateau(plateau);

            Statu statu = new Statu();
            statu.setId(scene.getId_statu());
            sc.setStatu(statu);

            Auteur auteur = new Auteur();
            auteur.setId(scene.getId_statu());
            sc.setAuteur(auteur);

            dao.save(sc);

            V_scene c = new V_scene();
            c.setId_plateau(scene.getId_plateau());
            Object[] element = dao.where(c);
            V_scene[] listeScene = new V_scene[element.length];
            for (int i = 0; i < element.length; i++) {
                V_scene ecole = (V_scene) element[i];
                listeScene[i] = ecole;
            }
            Object[] plateau_element = dao.findAll(new Plateau());
            Plateau[] listePlateau = new Plateau[plateau_element.length];
            for (int i = 0; i < plateau_element.length; i++) {
                Plateau p = (Plateau) plateau_element[i];
                listePlateau[i] = p;
            }
            Object[] auteur_element = dao.findAll(new Auteur());
            Auteur[] listeAuteur = new Auteur[auteur_element.length];
            for (int i = 0; i < auteur_element.length; i++) {
                Auteur a1 = (Auteur) auteur_element[i];
                listeAuteur[i] = a1;
            }
            model.addAttribute("listeAuteur", listeAuteur);
            model.addAttribute("listePlateau", listePlateau);
            model.addAttribute("listeScene", listeScene);
            model.addAttribute("id_film",scene.getId_film());
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return "listeScene";
    }

    @PostMapping("/deleteScene")
    public String deleteScene(@ModelAttribute V_scene scene,Model model){
        try {
            Scene sc = new Scene();
            sc.setId(scene.getId());
            dao.delete(sc);
            V_scene c = new V_scene();
            c.setId_plateau(scene.getId_plateau());
            Object[] element = dao.where(c);
            V_scene[] listeScene = new V_scene[element.length];
            for (int i = 0; i < element.length; i++) {
                V_scene ecole = (V_scene) element[i];
                listeScene[i] = ecole;
            }
            Object[] plateau_element = dao.findAll(new Plateau());
            Plateau[] listePlateau = new Plateau[plateau_element.length];
            for (int i = 0; i < plateau_element.length; i++) {
                Plateau p = (Plateau) plateau_element[i];
                listePlateau[i] = p;
            }
            Object[] auteur_element = dao.findAll(new Auteur());
            Auteur[] listeAuteur = new Auteur[auteur_element.length];
            for (int i = 0; i < auteur_element.length; i++) {
                Auteur a1 = (Auteur) auteur_element[i];
                listeAuteur[i] = a1;
            }
            model.addAttribute("listeAuteur", listeAuteur);
            model.addAttribute("listePlateau", listePlateau);
            model.addAttribute("listeScene", listeScene);
            model.addAttribute("id_film",scene.getId_film());
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return "listeScene";
    }

    @PostMapping("/terminerScene")
    public String terminerScene(@ModelAttribute Scene scene, Model model){
        try {
            SceneModel sss = new SceneModel();
            sss.setId(scene.getId());
            SceneModel temp = (SceneModel) dao.getById(sss);
            temp.setId_statu(3);
            dao.update(temp);

            V_scene c = new V_scene();
            c.setId_plateau(scene.getId_plateau());
            Object[] element = dao.where(c);
            V_scene[] listeScene = new V_scene[element.length];
            for (int i = 0; i < element.length; i++) {
                V_scene ecole = (V_scene) element[i];
                listeScene[i] = ecole;
            }
            Object[] plateau_element = dao.findAll(new Plateau());
            Plateau[] listePlateau = new Plateau[plateau_element.length];
            for (int i = 0; i < plateau_element.length; i++) {
                Plateau p = (Plateau) plateau_element[i];
                listePlateau[i] = p;
            }
            Object[] auteur_element = dao.findAll(new Auteur());
            Auteur[] listeAuteur = new Auteur[auteur_element.length];
            for (int i = 0; i < auteur_element.length; i++) {
                Auteur a1 = (Auteur) auteur_element[i];
                listeAuteur[i] = a1;
            }
            model.addAttribute("listeAuteur", listeAuteur);
            model.addAttribute("listePlateau", listePlateau);
            model.addAttribute("listeScene", listeScene);
            model.addAttribute("id_film",scene.getId_film());
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return "listeScene";
    }

}
