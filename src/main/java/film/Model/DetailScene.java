package film.Model;

import javax.persistence.*;

@Entity
@Table(name="detailScene")
public class DetailScene {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(name = "id_scene")
    public int id_scene;

    @Column(name = "id_type_action")
    public int id_type_action;

    @Column(name = "id_personnage")
    public int id_personnage;

    @Column(name = "intitule")
    public String intitule;

    @Column(name = "id_emotion")
    public int id_emotion;

    @Column(name = "duree")
    public int duree;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_scene() {
        return id_scene;
    }

    public void setId_scene(int id_scene) {
        this.id_scene = id_scene;
    }

    public int getId_type_action() {
        return id_type_action;
    }

    public void setId_type_action(int id_type_action) {
        this.id_type_action = id_type_action;
    }

    public int getId_personnage() {
        return id_personnage;
    }

    public void setId_personnage(int id_personnage) {
        this.id_personnage = id_personnage;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public int getId_emotion() {
        return id_emotion;
    }

    public void setId_emotion(int id_emotion) {
        this.id_emotion = id_emotion;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }
}
