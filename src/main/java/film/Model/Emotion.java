package film.Model;

import javax.persistence.*;

@Entity
@Table(name="emotion")
public class Emotion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(name = "nom_emotion")
    public String nom_emotion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom_emotion() {
        return nom_emotion;
    }

    public void setNom_emotion(String nom_emotion) {
        this.nom_emotion = nom_emotion;
    }
}
