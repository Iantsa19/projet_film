package film.Model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class JourPlanning {
    public List<V_scene> scenes;
    public final int MAX_DUREE = 200;
    public Date date;
    public int resteDuree = MAX_DUREE;

    public List<V_scene> getScenes() {
        return scenes;
    }

    public void setScenes(List<V_scene> scenes) {
        this.scenes = scenes;
    }

    public void setOldScenes(V_scene[] olds) {
        List<V_scene> oldScenes = new ArrayList<V_scene>();
        for (int i = 0; i < olds.length; i++) {
            oldScenes.add(olds[i]);
            this.resteDuree -= olds[i].getDuree();
        }
        this.scenes = oldScenes;
    }

    public int getMAX_DUREE() {
        return MAX_DUREE;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getResteDuree() {
        return resteDuree;
    }

    public void setResteDuree(int resteDuree) {
        this.resteDuree = resteDuree;
    }

}
