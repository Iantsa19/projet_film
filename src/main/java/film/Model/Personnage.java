package film.Model;

import javax.persistence.*;

@Entity
@Table(name="personnage")
public class Personnage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(name = "nom_acteur")
    public String nom_acteur;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom_acteur() {
        return nom_acteur;
    }

    public void setNom_acteur(String nom_acteur) {
        this.nom_acteur = nom_acteur;
    }
}
