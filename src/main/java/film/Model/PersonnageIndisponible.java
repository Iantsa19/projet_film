package film.Model;

import javax.persistence.*;
import java.sql.Date;

@Entity(name="Personnage_indisponible")
@Table(name="personnage_indisponible")
public class PersonnageIndisponible {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "date")
    private Date date;

    @Column(name = "id_personnage")
    private int id_personnage;

    @Column(name = "observation")
    private String observation;

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_personnage() {
        return id_personnage;
    }

    public void setId_personnage(int id_personnage) {
        this.id_personnage = id_personnage;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
