package film.Model;

import javax.persistence.*;

@Entity
@Table(name="plateau")
public class Plateau {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(name = "nom_plateau")
    public String nom_plateau;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom_plateau() {
        return nom_plateau;
    }

    public void setNom_plateau(String nom_plateau) {
        this.nom_plateau = nom_plateau;
    }
}
