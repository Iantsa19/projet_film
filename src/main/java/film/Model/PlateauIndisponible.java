package film.Model;

import javax.persistence.*;
import java.sql.Date;

@Entity(name="Plateau_indisponible")
@Table(name="plateau_indisponible")
public class PlateauIndisponible {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

//    @ManyToOne
//    @JoinColumn(name = "id_plateau")
//    public Plateau plateau;

    @Column(name = "date")
    private Date date;

    @Column(name = "id_plateau")
    private int id_plateau;

    @Column(name = "observation")
    private String observation;

    public String getObservation() {
        return observation;
    }

//    public Plateau getPlateau() {
//        return plateau;
//    }
//
//    public void setPlateau(Plateau plateau) {
//        this.plateau = plateau;
//    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_plateau() {
        return id_plateau;
    }

    public void setId_plateau(int id_plateau) {
        this.id_plateau = id_plateau;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
