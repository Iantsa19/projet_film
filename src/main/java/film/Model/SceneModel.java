package film.Model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name="scene")
public class SceneModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(name = "nom_scene")
    public String nom_scene;

    @Column(name = "id_film")
    public int id_film;

    @Column(name = "id_plateau")
    public int id_plateau;

    @Column(name = "id_statu")
    public int id_statu;

    @Column(name = "date_tournage")
    public Date date_tournage;

    public Date getDate_tournage() {
        return date_tournage;
    }

    public void setDate_tournage(Date date_tournage) {
        this.date_tournage = date_tournage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom_scene() {
        return nom_scene;
    }

    public void setNom_scene(String nom_scene) {
        this.nom_scene = nom_scene;
    }

    public int getId_film() {
        return id_film;
    }

    public void setId_film(int id_film) {
        this.id_film = id_film;
    }

    public int getId_plateau() {
        return id_plateau;
    }

    public void setId_plateau(int id_plateau) {
        this.id_plateau = id_plateau;
    }

    public int getId_statu() {
        return id_statu;
    }

    public void setId_statu(int id_statu) {
        this.id_statu = id_statu;
    }
}
