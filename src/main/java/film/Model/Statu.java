package film.Model;

import javax.persistence.*;

@Entity(name = "statu")
@Table(name="statu")
public class Statu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(name = "nom_statu")
    public String nom_statu;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom_statu() {
        return nom_statu;
    }

    public void setNom_statu(String nom_statu) {
        this.nom_statu = nom_statu;
    }

}
