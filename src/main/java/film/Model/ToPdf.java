package film.Model;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class ToPdf {
    Document document = new Document();
    public ToPdf(JourPlanning[] plan){
        try {
            PdfWriter.getInstance(document, new FileOutputStream("D:/Planning-"+ plan[0].getDate() +".pdf"));
            document.open();

            Font font = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD);
            Paragraph p = new Paragraph("Planning général",font);
            p.setAlignment(Element.ALIGN_CENTER);
            p.setSpacingAfter(20);
            document.add(p);

            document.add(new Paragraph("Début du tournage: "+ plan[0].getDate() ));
            document.add(new Paragraph("Fin du tournage: "+ plan[plan.length - 1].getDate()));




            Font f = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
            Paragraph pa = new Paragraph("Planning de tournage",f);
            pa.setAlignment(Element.ALIGN_LEFT);
            pa.setSpacingBefore(20);
            document.add(pa);

            for(int i=0;i< plan.length;i++){
                for(int j=0;j<plan[i].getScenes().size();j++) {


                    Font font_date = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
                    Paragraph date = new Paragraph("Date: "+ plan[0].getDate(),font_date);
                    date.setAlignment(Element.ALIGN_LEFT);
                    date.setSpacingBefore(20);
                    document.add(date);

                    Font scene_plat = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.UNDERLINE);
                    Paragraph scene_plateau = new Paragraph( plan[i].getScenes().get(j).getNom_scene() +"-"+ plan[i].getScenes().get(j).getPlateau().getNom_plateau(),scene_plat );
                    scene_plateau.setAlignment(Element.ALIGN_LEFT);
                    scene_plateau.setSpacingBefore(10);
                    scene_plateau.setSpacingAfter(15);
                    document.add(scene_plateau);

                    int x=1;
                    for(V_detail det:plan[i].getScenes().get(j).getDetails()) {
                        Font deta = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
                        Paragraph detail = new Paragraph("Détail " + (x) + " : " + det.getNom_acteur(),deta);
                        detail.setAlignment(Element.ALIGN_LEFT);
                        document.add(detail);

                        Font t = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
                        Paragraph pt = new Paragraph("Texte " + (x) + " : " + det.getIntitule(),t);

                        pt.setIndentationLeft(48);
                        pt.setSpacingAfter(10);
                        document.add(pt);
                        x+=1;
                    }


                }
            }

            document.close();
            System.out.println("PDF generated successfully.");
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
