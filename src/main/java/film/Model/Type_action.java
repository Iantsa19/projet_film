package film.Model;

import javax.persistence.*;

@Entity
@Table(name="type_action")
public class Type_action {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(name = "nom_type_action")
    public String nom_type_action;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom_type_action() {
        return nom_type_action;
    }

    public void setNom_type_action(String nom_type_action) {
        this.nom_type_action = nom_type_action;
    }
}
