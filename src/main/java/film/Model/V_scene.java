package film.Model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name="v_scene")
public class V_scene {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @ManyToOne
    @JoinColumn(name = "id_film")
    public Film film;

    @Transient
    public int id_film;

    @Column(name = "nom_scene")
    public String nom_scene;

    @ManyToOne
    @JoinColumn(name = "id_plateau")
    public Plateau plateau;

    @Transient
    public int id_plateau;

    @Column(name = "duree")
    public Integer duree;

    @ManyToOne
    @JoinColumn(name = "id_statu")
    public Statu statu;

    @Transient
    public int id_statu;

    @Transient
    public int id_auteur;

    @ManyToOne
    @JoinColumn(name = "id_auteur")
    public Auteur auteur;

    public Statu getStatu() {
        return statu;
    }

    public void setStatu(Statu statu) {
        this.statu = statu;
    }

    @Column(name = "date_tournage")
    public Date date_tournage;
    @Transient
    public List<DetailScene> detail;
    @Transient
    public List<V_detail> details;

    public List<V_detail> getDetails() {
        return details;
    }

    public void setDetails(List<V_detail> details) {
        this.details = details;
    }

    public List<DetailScene> getDetail() {
        return detail;
    }

    public void setDetail(List<DetailScene> detail) {
        this.detail = detail;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public int getId_auteur() {
        return id_auteur;
    }

    public void setId_auteur(int id_auteur) {
        this.id_auteur = id_auteur;
    }

    public Auteur getAuteur() {
        return auteur;
    }

    public void setAuteur(Auteur auteur) {
        this.auteur = auteur;
    }

    @Transient
    public Personnage[] personnages;

    public Personnage[] getPersonnages() {
        return personnages;
    }

    public void setPersonnages(Personnage[] personnages) {
        this.personnages = personnages;
    }

    public Date getDate_tournage() {
        return date_tournage;
    }

    public void setDate_tournage(Date date_tournage) {
        this.date_tournage = date_tournage;
    }

    public int getId_statu() {
        return id_statu;
    }

    public void setId_statu(int id_statu) {
        this.id_statu = id_statu;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public int getId_film() {
        return id_film;
    }

    public void setId_film(int id_film) {
        this.id_film = id_film;
    }


    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom_scene() {
        return nom_scene;
    }

    public void setNom_scene(String nom_scene) {
        this.nom_scene = nom_scene;
    }

    public int getId_plateau() {
        return id_plateau;
    }

    public void setId_plateau(int id_plateau) {
        this.id_plateau = id_plateau;
    }

    public Integer getDuree() {
        return duree;
    }

    public void setDuree(Integer duree) {
        if(duree==null){
            duree=0;
        }
        this.duree = duree;
    }
}
