<%--
  Created by IntelliJ IDEA.
  User: mahery
  Date: 3/13/23
  Time: 10:02 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="film.Model.V_scene" %>
<%@ page import="film.Model.Plateau" %>
<%@ page import="film.Model.Auteur" %>
<%
    V_scene[] listeScene = (V_scene[]) request.getAttribute("listeScene");
    Plateau[] listePlateau = (Plateau[]) request.getAttribute("listePlateau");
    Auteur[] listeAuteur = (Auteur[]) request.getAttribute("listeAuteur");
    int id_film = (int) request.getAttribute("id_film");
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Plateau</title>

    <!-- Bootstrap -->
    <link href="<%= request.getContextPath() %>/ressources/theme/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%= request.getContextPath() %>/ressources/theme/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%= request.getContextPath() %>/ressources/theme/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<%= request.getContextPath() %>/ressources/theme/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<%= request.getContextPath() %>/ressources/theme/build/css/custom.min.css" rel="stylesheet">
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Film Maker</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="images/img.jpg" alt="..." class="img-circle profile_img">
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />
                <jsp:include page="navbar.jsp"/>
            </div>
        </div>
        <jsp:include page="top_navigation.jsp"/>
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Liste des scenes du plateau</h3>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel" style="">
                            <div class="x_title">
                                <h2>Inserer un nouveau scene</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <div class="container">
                                    <form action="<%= request.getContextPath() %>/Scene" method="post">
                                        <div class="row">
                                            <div class='col-sm-3'>
                                                Nom scene
                                                <div class="form-group">
                                                    <input type='text' class="form-control" name="nom_scene"/>
                                                </div>
                                            </div>
                                            <div class='col-sm-3'>
                                                Plateau
                                                <div class="form-group">
                                                    <div class='input-group date' id='myDatepicker'>
                                                        <select name="id_plateau" class="form-control">
                                                            <%
                                                                for(int i=0;i<listePlateau.length;i++){ %>
                                                            <option value="<%= listePlateau[i].getId() %>"><%= listePlateau[i].getNom_plateau() %></option>
                                                            <% }
                                                            %>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-sm-3'>
                                                Auteur
                                                <div class="form-group">
                                                    <div class='input-group date' id='myDatepicker2'>
                                                        <select name="id_auteur" class="form-control">
                                                            <%
                                                                for(int i=0;i<listeAuteur.length;i++){ %>
                                                            <option value="<%= listeAuteur[i].getId() %>"><%= listeAuteur[i].getNom_auteur() %></option>
                                                            <% }
                                                            %>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="id_statu" value="1">
                                            <input type="hidden" name="id_film" value="<%= id_film %>">
                                            <div class='col-sm-3'>
                                                <br/>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Inserer">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="display: block;">
                    <div class="col-md-12 col-sm-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Liste Scene</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#">Settings 1</a>
                                            <a class="dropdown-item" href="#">Settings 2</a>
                                        </div>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nom Scene</th>
                                        <th>Plateau</th>
                                        <th>Auteur</th>
                                        <th>Statu</th>
                                        <th>Duree</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <%
                                        if(listeScene!=null){
                                            for(int i=0;i<listeScene.length;i++){ %>
                                    <tr>
                                        <th scope="row"><%= listeScene[i].getId() %></th>
                                        <td><%= listeScene[i].getNom_scene() %></td>
                                        <td><%= listeScene[i].getPlateau().getNom_plateau() %></td>
                                        <td><%= listeScene[i].getAuteur().getNom_auteur() %></td>
                                        <td><%= listeScene[i].getStatu().getNom_statu() %></td>
                                        <td><%= listeScene[i].getDuree() %> secondes</td>
                                        <form action="<%= request.getContextPath() %>/DetailScene" method="post">
                                            <input type="hidden" name="id_scene" value="<%= listeScene[i].getId() %>">
                                            <td><input type="submit" class="btn btn-info" value="Detail"></td>
                                        </form>
                                        <form action="<%= request.getContextPath() %>/deleteScene" method="post">
                                            <input type="hidden" name="id" value="<%= listeScene[i].getId() %>">
                                            <td><input type="submit" class="btn btn-danger" value="Supprimer"></td>
                                        </form>
                                        <%
                                            if(listeScene[i].getStatu().getId()==2){ %>
                                        <form action="<%= request.getContextPath() %>/terminerScene" method="post">
                                            <input type="hidden" name="id" value="<%= listeScene[i].getId() %>">
                                            <td><input type="submit" class="btn btn-success" value="Terminer"></td>
                                        </form>
                                        <% }
                                        %>
                                    </tr>
                                    <% }
                                    }
                                    %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
    <jsp:include page="footer.jsp"/>
</div>
</div>
<!-- jQuery -->
<script src="<%= request.getContextPath() %>/ressources/theme/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<%= request.getContextPath() %>/ressources/theme/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="<%= request.getContextPath() %>/ressources/theme/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<%= request.getContextPath() %>/ressources/theme/vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="<%= request.getContextPath() %>/ressources/theme/vendors/iCheck/icheck.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="<%= request.getContextPath() %>/ressources/theme/build/js/custom.min.js"></script>
</body>
</html>
