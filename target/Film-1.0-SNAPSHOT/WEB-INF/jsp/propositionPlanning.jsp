<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="film.Model.JourPlanning" %>
<%@ page import="java.util.List" %>
<%@ page import="film.Model.V_scene" %>
<%
    JourPlanning[] dates = (JourPlanning[]) request.getAttribute("dates");
    List<V_scene> scenes = (List<V_scene>) request.getAttribute("scenesAplanifier");
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Planning</title>

    <!-- Bootstrap -->
    <link href="<%= request.getContextPath() %>/ressources/theme/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%= request.getContextPath() %>/ressources/theme/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%= request.getContextPath() %>/ressources/theme/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<%= request.getContextPath() %>/ressources/theme/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<%= request.getContextPath() %>/ressources/theme/build/css/custom.min.css" rel="stylesheet">

    <style>

        /* Positionnez la forme Popup */
        .login-popup {
            position: relative;
            text-align: center;
            width: 100%;
        }
        /* Masquez la forme Popup */
        .form-popup {
            display: none;
            position: fixed;
            left: 45%;
            top: 5%;
            transform: translate(-45%, 5%);
            border: 2px solid #666;
            z-index: 9;
        }
        /* Styles pour le conteneur de la forme */
        .form-container {
            max-width: 500px;
            padding: 20px;
            background-color: #fff;
        }
    </style>
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Film Maker</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="images/img.jpg" alt="..." class="img-circle profile_img">
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />
                <jsp:include page="navbar.jsp"/>
            </div>
        </div>
        <jsp:include page="top_navigation.jsp"/>
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Planning du <%= dates[0].getDate() %> au <%= dates[dates.length - 1].getDate() %></h3>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <% for (int i = 0; i < dates.length; i++) { %>
                    <div class="col-md-6">
                        <div class="x_panel" style="">
                            <div class="x_title">
                                <h2><%= dates[i].getDate() %></h2>
                                <p class="col-sm-4 pt-2" style="color: red">Durée restantes: <%= dates[i].getResteDuree() %> sec</p>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <div class="container">
                                    <form method="post" action="<%= request.getContextPath() %>/valider-planning">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Plateau</th>
                                            <th>ID</th>
                                            <th>Nom Scene</th>
                                            <th>Duree</th>
                                            <th>Check</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <%
                                            if(dates[i].getScenes()!=null){
                                                for(int j=0;j<dates[i].getScenes().size();j++){ %>
                                        <tr>
                                            <th scope="row"><%= dates[i].getScenes().get(j).getPlateau().getNom_plateau() %></th>
                                            <td><%= dates[i].getScenes().get(j).getId() %></td>
                                            <td><%= dates[i].getScenes().get(j).getNom_scene() %></td>
                                            <td><%= dates[i].getScenes().get(j).getDuree() %> secondes</td>
                                            <% if (dates[i].getScenes().get(j).getStatu().getId() == 3 || dates[i].getScenes().get(j).getId_statu() == 3) { %>
                                            <td><input type="checkbox" name="j[]" value="<%= j %>">
                                            <input type="hidden" name="id_plateau[]" value="<%= dates[i].getScenes().get(j).getPlateau().getId() %>">
                                            <input type="hidden" name="i" value="<%= i %>">
                                            </td>
                                            <td>
<%--                                                <a href="#" class="btn btn-outline-primary btn-sm"><i class="fa fa-eye"></i> </a>--%>
<%--                                                <a href="#" class="btn btn-outline-info btn-sm"><i class="fa fa-pencil"></i> </a>--%>
                                                <a class="btn btn-outline-danger btn-sm" href="<%= request.getContextPath() %>/remove-scene-to-date/<%= i %>/<%= j %>"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                            <% } %>
                                        </tr>
                                        <% }
                                        }
                                        %>
                                        </tbody>
                                    </table>

                                    <div class='row mt-3'>
                                        <div class="form-group col-3">
                                            <button class="btn btn-outline-secondary w-100" type="button" onclick="openForm(<%= i %>)">
                                                <i class="fa fa-plus"></i>
                                                scene
                                            </button>
                                        </div>
                                        <div class="form-group col-3">
                                            <input type="submit" class="btn btn-primary w-100" value="Valider">
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                <div class="login-popup">
                                    <div class="form-popup" id="popupForm">
                                        <form class="form-container" action="<%= request.getContextPath() %>/add-scene-to-date" method="post">
                                            <div id="indexDate"></div>
                                            <h2>Choisir les scènes</h2>
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Check</th>
                                                    <th>Plateau</th>
                                                    <th>ID</th>
                                                    <th>Nom Scene</th>
                                                    <th>Duree</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <%
                                                    if(scenes!=null){
                                                        for(int j=0;j<scenes.size();j++){ %>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="j[]" value="<%= j %>">
                                                    </td>
                                                    <th scope="row"><%= scenes.get(j).getPlateau().getNom_plateau() %></th>
                                                    <td><%= scenes.get(j).getId() %></td>
                                                    <td><%= scenes.get(j).getNom_scene() %></td>
                                                    <td><%= scenes.get(j).getDuree() %> secondes</td>
                                                </tr>
                                                <% }
                                                }
                                                %>
                                                </tbody>
                                            </table>
                                            <div class="row mt-2">
                                                <button type="button" class="btn btn-secondary" onclick="closeForm()">Fermer</button>
                                                <button type="submit" class="btn btn-primary">Ajouter</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <script>
                                    function openForm(index) {
                                        document.getElementById("indexDate").innerHTML = "<input type='hidden' value=" + index + " name='indexDate'>"
                                        document.getElementById("popupForm").style.display = "block";
                                    }

                                    function closeForm() {
                                        document.getElementById("popupForm").style.display = "none";
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
    <jsp:include page="footer.jsp"/>
</div>
</div>
<!-- jQuery -->
<script src="<%= request.getContextPath() %>/ressources/theme/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<%= request.getContextPath() %>/ressources/theme/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="<%= request.getContextPath() %>/ressources/theme/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<%= request.getContextPath() %>/ressources/theme/vendors/nprogress/nprogress.js"></script>
<!-- iCheck -->
<script src="<%= request.getContextPath() %>/ressources/theme/vendors/iCheck/icheck.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="<%= request.getContextPath() %>/ressources/theme/build/js/custom.min.js"></script>
</body>
</html>
