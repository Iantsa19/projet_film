CREATE DATABASE film;
CREATE USER film WITH PASSWORD 'film';
ALTER DATABASE film OWNER to film;

CREATE TABLE Film (
    id SERIAL PRIMARY KEY,
    titre VARCHAR(100) not null
);

insert into film (titre) values ('Malok''ila Grand retour');

CREATE TABLE Plateau (
    id SERIAL PRIMARY KEY,
    nom_plateau VARCHAR(100) not null
);

insert into Plateau(nom_plateau) values('Plateau 1');
insert into Plateau(nom_plateau) values('Plateau 2');
insert into Plateau(nom_plateau) values('Plateau 3');
insert into Plateau(nom_plateau) values('Plateau 4');
insert into Plateau(nom_plateau) values('Plateau 5');
insert into Plateau(nom_plateau) values('Plateau 6');
insert into Plateau(nom_plateau) values('Plateau 7');
insert into Plateau(nom_plateau) values('Plateau 8');
insert into Plateau(nom_plateau) values('Plateau 9');
insert into Plateau(nom_plateau) values('Plateau 10');
insert into Plateau(nom_plateau) values('Plateau 11');
insert into Plateau(nom_plateau) values('Plateau 12');
insert into Plateau(nom_plateau) values('Plateau 13');
insert into Plateau(nom_plateau) values('Plateau 14');
insert into Plateau(nom_plateau) values('Plateau 15');

CREATE TABLE Personnage (
    id SERIAL PRIMARY KEY,
    nom_acteur VARCHAR(100) not null
);

insert into Personnage(nom_acteur) values('Personnage 1');
insert into Personnage(nom_acteur) values('Personnage 2');
insert into Personnage(nom_acteur) values('Personnage 3');
insert into Personnage(nom_acteur) values('Personnage 4');
insert into Personnage(nom_acteur) values('Personnage 5');

CREATE TABLE Type_action (
    id SERIAL PRIMARY KEY,
    nom_type_action VARCHAR(100) not null
);
INSERT INTO Type_action (nom_type_action) values ('Dialogue');
INSERT INTO Type_action (nom_type_action) values ('Action');

CREATE TABLE Statu (
    id SERIAL PRIMARY KEY,
    nom_statu VARCHAR(100) not null
);
INSERT INTO Statu (nom_statu) values ('Creer');
INSERT INTO Statu (nom_statu) values ('En cours');
INSERT INTO Statu (nom_statu) values ('Termine');
INSERT INTO Statu (nom_statu) values ('Planifie');

CREATE TABLE Auteur (
    id SERIAL PRIMARY KEY,
    nom_auteur VARCHAR(100) not null
);

INSERT INTO auteur (nom_auteur) VALUES ('Auteur 1');
INSERT INTO auteur (nom_auteur) VALUES ('Auteur 2');
INSERT INTO auteur (nom_auteur) VALUES ('Auteur 3');
INSERT INTO auteur (nom_auteur) VALUES ('Auteur 4');
INSERT INTO auteur (nom_auteur) VALUES ('Auteur 5');

CREATE TABLE Scene (
    id SERIAL PRIMARY KEY,
    id_film int not null references Film(id),
    nom_scene VARCHAR(100) not null,
    id_plateau int not null references Plateau(id),
    id_statu int not null references Statu(id),
    id_auteur int not null references auteur(id),
    date_tournage date
);

insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 1',1,3,1);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 2',2,3,2);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 3',3,3,3);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 4',4,3,4);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 5',5,3,5);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 6',6,1,1);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 7',7,1,2);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 8',8,1,3);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 9',9,1,4);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 10',10,1,5);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 11',11,1,1);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 12',12,1,2);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 13',13,1,3);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 14',14,1,4);
insert into Scene(id_film,nom_scene,id_plateau, id_statu, id_auteur) values(1,'Scene 15',15,1,5);


CREATE TABLE Emotion (
    id SERIAL PRIMARY KEY,
    nom_emotion VARCHAR(100)
);

insert into Emotion(nom_emotion) values('Emotion 1');
insert into Emotion(nom_emotion) values('Emotion 2');
insert into Emotion(nom_emotion) values('Emotion 3');
insert into Emotion(nom_emotion) values('Emotion 4');
insert into Emotion(nom_emotion) values('Emotion 5');
insert into Emotion(nom_emotion) values('Emotion 6');
insert into Emotion(nom_emotion) values('Emotion 7');

CREATE TABLE DetailScene (
    id SERIAL PRIMARY KEY,
    id_scene int not null references Scene(id),
    id_type_action int not null references Type_action(id),
    id_personnage int references Personnage(id),
    intitule TEXT,
    id_emotion int references Emotion(id),
    duree int check (duree>=0)
);

insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(1,1,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(1,1,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',2,10);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(1,2,2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1,5);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(1,1,2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',3,17);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(1,1,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',4,10);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(1,1,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',7,4);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(1,1,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',3,2);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(1,2,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',2,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(2,1,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',6,10);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(2,1,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',6,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(2,2,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(2,1,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1,11);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(2,1,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',6,10);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(2,1,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',6,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(2,2,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',2,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(2,1,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',7,11);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(3,1,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',7,10);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(3,1,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',4,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(3,2,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',4,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(3,1,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1,11);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(3,1,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1,10);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(3,1,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',2,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(3,2,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',2,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(3,1,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',6,11);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(4,1,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',3,10);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(4,1,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',3,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(4,2,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(4,1,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',2,11);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(4,1,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',4,10);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(4,1,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',7,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(4,2,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',6,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(4,1,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1,11);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(5,1,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',7,10);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(5,1,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',6,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(5,2,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',2,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(5,1,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1,11);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(5,1,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',5,10);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(5,1,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(5,2,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1,12);
insert into DetailScene(id_scene,id_type_action,id_personnage,intitule,id_emotion,duree) values(5,1,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',7,11);

CREATE TABLE Plateau_indisponible (
    id SERIAL PRIMARY KEY,
    id_plateau int not null references Plateau(id),
    observation TEXT,
    date date not null
);

insert into Plateau_indisponible (id_plateau,observation,date) values (2,'Travaux en cours','2023-03-22');
insert into Plateau_indisponible (id_plateau,observation,date) values (3,'Travaux en cours','2023-03-22');
insert into Plateau_indisponible (id_plateau,observation,date) values (4,'Travaux en cours','2023-03-22');

CREATE TABLE Personnage_indisponible (
    id SERIAL PRIMARY KEY,
    id_personnage int not null references Personnage(id),
    observation TEXT,
    date date not null
);

insert into Personnage_indisponible (id_personnage,observation,date) values (1,'Indisponible','2023-03-23');
insert into Personnage_indisponible (id_personnage,observation,date) values (2,'En vacances','2023-03-22');
insert into Personnage_indisponible (id_personnage,observation,date) values (3,'Déménagement','2023-03-24');
insert into Personnage_indisponible (id_personnage,observation,date) values (4,'Consultation','2023-03-22');

/*CREATE TABLE Scene_planifie (
    id SERIAL PRIMARY KEY,
    id_scene int not null references Scene(id),
    date_tournage date not null
);*/

CREATE or REPLACE View v_scene as
    select Scene.*,
    CASE
        WHEN sum(DetailScene.duree) IS NULL THEN 0
        ELSE sum(DetailScene.duree)
        END as duree from Scene
    full join DetailScene on DetailScene.id_scene=Scene.id
    GROUP BY Scene.id order by id_plateau,id;

CREATE or REPLACE View v_detail as
    select DetailScene.*,Type_action.nom_type_action,Personnage.nom_acteur,Emotion.nom_emotion
    from DetailScene
    join Type_action on DetailScene.id_type_action=Type_action.id
    full join Personnage on DetailScene.id_personnage=Personnage.id
    full join Emotion on DetailScene.id_emotion=Emotion.id
    order by id;
